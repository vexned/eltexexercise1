package vladoff.space.lab6.Client;

import vladoff.space.lab6.OrderStuff.*;
import vladoff.space.lab6.Products.Coffee;
import vladoff.space.lab6.Products.Product;
import vladoff.space.lab6.Products.Tea;

import java.util.Random;

public class Helper<T extends Order> extends Thread {

    private final static int MAX_ITEMS_IN_CART = 5;
    private final static Random random = new Random();
    Orders<T> orders;
    long interval;

    public Helper(Orders<T> orders, long interval) {
        this.orders = orders;
        this.interval = interval;
    }

    @Override
    public void run() {
        super.run();
        while (true) {
            Credentials credentials = new Credentials();
            credentials.create();
            ShoppingCart<Product> shoppingCart = new ShoppingCart<>();
            Product product;
            int itemsNumber = random.nextInt(MAX_ITEMS_IN_CART);
            for(int i = 0; i < itemsNumber; i++);
            {
                int productNumber = random.nextInt(2);
                if(productNumber==0)
                {
                    product = new Tea();
                    product.create();
                }
                else
                {
                    product = new Coffee();
                    product.create();
                }

                shoppingCart.add(product);
            }

            System.out.println("Order created! Order contains " + (itemsNumber+1) + " items.");

            //credentials.read();
            //shoppingCart.showAllObjects();

            orders.makePurchase(credentials, shoppingCart);

            try {
                sleep(interval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static public Order generateOrder() {
        Credentials credentials = new Credentials();
        credentials.create();
        ShoppingCart<Product> shoppingCart = new ShoppingCart<>();
        Product product;
        int itemsNumber = random.nextInt(MAX_ITEMS_IN_CART);
        for(int i = 0; i < itemsNumber; i++);
        {
            int productNumber = random.nextInt(2);
            if(productNumber==0)
            {
                product = new Tea();
                product.create();
            }
            else
            {
                product = new Coffee();
                product.create();
            }

            shoppingCart.add(product);
        }

        return new Order(credentials, Status.WAITING, Order.DEFAULT_DAYS_TO_WAIT, shoppingCart);
    }
}
