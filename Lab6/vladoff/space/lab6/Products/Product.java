package vladoff.space.lab6.Products;

import vladoff.space.lab6.Interfaces.ICrudAction;

import java.io.Serializable;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;
import java.util.UUID;


public class Product implements ICrudAction, Serializable {

    private final static transient String names[] = {"Утренний", "Вечерний", "Хороший", "Неплохой", "Крутой"};
    private final static transient String providers[] = {"CompanyCo", "CompCo", "CoffeGradesCo", "NiceAndCoolCompany"};
    private final static transient String countries[] = {"Россия", "Беларусь", "Казахстан", "Коста-Рика", "Испания", "Германия"};

    protected static final transient Random random = new Random();
    private UUID id;
    private String name;
    private double price;
    private static transient int productCounter;
    private String provider;
    private String country;

    public Product(){
        id = UUID.randomUUID();
    }

    @Override
    public void create() {
        productCounter++;
        name = names[random.nextInt(names.length)];
        price = random.nextDouble();
        country = countries[random.nextInt(countries.length)];
        provider = providers[random.nextInt(providers.length)];
    }

    @Override
    public void read() {
        System.out.println("Наименование: " + name);
        System.out.println("Цена: " + price);
        System.out.println("Поставщик: " + provider);
        System.out.println("Страна: " + country);
        System.out.println("Общее число товаров:" + productCounter);
    }

    @Override
    public void update() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите наименование: ");
        name = scanner.next();
        System.out.print("Введите цену: ");
        price = scanner.nextDouble();
        System.out.print("Введите поставщика: ");
        provider = scanner.next();
        System.out.print("Введите страну: ");
        country = scanner.next();
    }

    @Override
    public void delete() {
        productCounter--;
        name = "";
        id = null;
        price = 0.0d;
        provider = "";
        country = "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Double.compare(product.price, price) == 0 &&
                Objects.equals(id, product.id) &&
                Objects.equals(name, product.name) &&
                Objects.equals(provider, product.provider) &&
                Objects.equals(country, product.country);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, price, provider, country);
    }

    public UUID getId() {
        return id;
    }
}
