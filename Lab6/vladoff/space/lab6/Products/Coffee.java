package vladoff.space.lab6.Products;

import java.util.Objects;
import java.util.Scanner;

public final class Coffee extends Product {

    private final static transient String coffeeGrades[] = {"Арабика", "Робуста", "Либерика", "Эксельсо"};

    private String coffeeGrade;

    public Coffee() {
        super();
    }


    @Override
    public void create() {
        super.create();
        coffeeGrade = coffeeGrades[random.nextInt(coffeeGrades.length)];
    }

    @Override
    public void read() {
        super.read();
        System.out.println("Сорт кофе: " + coffeeGrade);
    }

    @Override
    public void update() {
        super.update();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите сорт кофе: ");
        coffeeGrade = scanner.next();
        scanner.close();
    }

    @Override
    public void delete() {
        super.delete();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Coffee coffee = (Coffee) o;
        return Objects.equals(coffeeGrade, coffee.coffeeGrade);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), coffeeGrade);
    }
}
