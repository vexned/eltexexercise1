package vladoff.space.lab6.Server;

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

import vladoff.space.lab6.OrderStuff.Orders;
import vladoff.space.lab6.OrderStuff.Order;
import vladoff.space.lab6.OrderStuff.Status;

public class Server implements Runnable {

    private static final Integer[] DEFAULT_UDP_PORTS_TO_NOTIFY = {6666, 6667, 6668, 6669, 6670, 6671, 6672, 6673, 6674};
    private static final Integer TCP_RECEIVE_PORT = 7777;
    private static final String DEFAULT_HOST = "localhost";
    private static final Integer DEFAULT_BACKLOG = 20;

    private String host;
    private Integer tcpPort;
    private Map<Integer, Orders<Order>> ordersFromPorts;
    private Orders<Order> allOrders;
    private Queue<Integer> portsToSendNotificationQueue;

    public Server(String host) {
        this.host = host;
        this.tcpPort = TCP_RECEIVE_PORT;
        ordersFromPorts = Collections.synchronizedMap(new HashMap<>());
        allOrders = new Orders<>();
        portsToSendNotificationQueue = new ConcurrentLinkedQueue<>();
        portsToSendNotificationQueue.addAll(Arrays.asList(DEFAULT_UDP_PORTS_TO_NOTIFY));
    }

    public Server() {
        this(DEFAULT_HOST);
    }

    //UDP-client sends notifications to server's clients
    //This Thread, Maybe.
    private void sendPortNotification(int udpPortToNotify) {
        //Notify order status, tcp port to send orders
        //Check if port is available
        boolean aval = false;
        int localPortVar;
        synchronized (tcpPort) {
            while (!aval) {
                aval = available(tcpPort);
                tcpPort++;
            }
            localPortVar = tcpPort - 1;
        }

        //Open TCP-Server
        receiveOrders(localPortVar, udpPortToNotify);

        //Send Notification with port(UDP Client)
        try (DatagramSocket socket = new DatagramSocket()) {
            socket.setBroadcast(true);
            socket.setSoTimeout(10000);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(localPortVar);
            byte[] localPortByteArray = baos.toByteArray();
            DatagramPacket request = new DatagramPacket(localPortByteArray, 0, localPortByteArray.length, InetAddress.getByName("255.255.255.255"), udpPortToNotify);
            socket.send(request);

            System.out.println("[SERVER]ВЫСЫЛАЕМ ПОРТ " + udpPortToNotify);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private void sendStatusNotification(UUID orderId, int udpPortToNotify) {
        try (DatagramSocket socket = new DatagramSocket()) {
            socket.setSoTimeout(10000);
            socket.setBroadcast(true);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(orderId.toString());
            byte[] statusBytes = baos.toByteArray();
            DatagramPacket request = new DatagramPacket(statusBytes, 0, statusBytes.length, InetAddress.getByName("255.255.255.255"), udpPortToNotify);
            socket.send(request);
            System.out.println("[SERVER]Отправили статус заказа: " + orderId.toString());

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    //TCP-server receives orders from clients
    //New Thread
    private void receiveOrders(int currentTcpPort, int udpNotifyPort) {
        Thread tcpServer = new Thread(() -> {

            try (ServerSocket connectionSocket = new ServerSocket(currentTcpPort)) {
                Socket message = connectionSocket.accept();
                System.out.println("[SERVER]КЛИЕНТ СОЕДИНИЛСЯ НА " + currentTcpPort);
                ObjectInputStream ois = new ObjectInputStream(message.getInputStream());
                Order order = (Order) ois.readObject();
                Orders<Order> orders = ordersFromPorts.get(udpNotifyPort);
                if (orders == null) {
                    orders = new Orders<>();
                    ordersFromPorts.put(udpNotifyPort, orders);
                }
                orders.add(order);
                allOrders.add(order);
                System.out.println("[SERVER]ПОЛУЧИЛИ ЗАКАЗ С " + currentTcpPort);
                message.close();
                synchronized (this.tcpPort) {
                    this.tcpPort--;
                }
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("FAILED TO CREATE TCP-SERVER AT " + host + ":" + currentTcpPort);
            } catch (ClassNotFoundException e) {
                System.out.println("FOLLOWING CLASS NOT FOUND");
                e.printStackTrace();
            }

        });

        tcpServer.start();
        //get orders from TCP-client
    }

    //Process orders in queue
    //New Thread
    private void processOrders(int interval) {
        Thread waitingCheck = new Thread(new WaitingCheck<>(allOrders, 20000));
        waitingCheck.start();

        Thread notifyOrdersProcessed = new Thread(() -> {
            while(true) {
                Set<Integer> keys = ordersFromPorts.keySet();
                Orders<Order> order;
                List<Order> orderList;
                for (int key : keys) {
                    order = ordersFromPorts.get(key);
                    orderList = order.getOrderList();
                    for (Order ord : orderList) {
                        if (ord.getStatus() == Status.PROCESSED) {
                            allOrders.remove(ord);
                            orderList.remove(ord);
                            sendStatusNotification(ord.getId(), key);
                            portsToSendNotificationQueue.add(key);
                        }
                    }
                }
                try {
                    Thread.sleep(interval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        notifyOrdersProcessed.start();
    }

    private void sendAllNotifications(int interval) {
        Thread send = new Thread(() -> {
            while (true) {
                Integer key = portsToSendNotificationQueue.poll();
                while (key != null) {
                    sendPortNotification(key);
                    key = portsToSendNotificationQueue.poll();
                }
                try {
                    Thread.sleep(interval);
                } catch (InterruptedException e) {
                    System.out.println(e.getMessage());
                }
            }
        });
        send.start();
    }

    public static boolean available(int port) {

        ServerSocket ss = null;
        DatagramSocket ds = null;
        try {
            ss = new ServerSocket(port);
            ss.setReuseAddress(true);
            ds = new DatagramSocket(port);
            ds.setReuseAddress(true);
            return true;
        } catch (IOException e) {
        } finally {
            if (ds != null) {
                ds.close();
            }

            if (ss != null) {
                try {
                    ss.close();
                } catch (IOException e) {
                    /* should not be thrown */
                }
            }
        }

        return false;
    }

    @Override
    public void run() {
        sendAllNotifications(20000);
        processOrders(10000);
    }
}
