package vladoff.space.lab6.Server;

import vladoff.space.lab6.OrderStuff.Order;
import vladoff.space.lab6.OrderStuff.Orders;

public abstract class ACheck<T extends Order> implements Runnable {


    protected Orders<T> orders;
    protected long interval;

    ACheck(Orders<T> orders, long interval) {
        this.orders = orders;
        this.interval = interval;
    }

    @Override
    public void run() {
        try {
            Process();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    abstract void Process() throws InterruptedException;

}
