package vladoff.space.lab6.Server;

import vladoff.space.lab6.OrderStuff.Order;
import vladoff.space.lab6.OrderStuff.Orders;

public class ProcessedCheck<T extends Order> extends ACheck<T> {

    ProcessedCheck(Orders<T> orders, int interval) {
        super(orders, interval);
    }


    @Override
    void Process() throws InterruptedException {

        while(true){
                orders.checkOrders();
                System.out.println("Обработка завершенных заказов.");
                Thread.sleep(interval);
                System.out.println("Завершенные заказы обработаны");
        }
    }
}
