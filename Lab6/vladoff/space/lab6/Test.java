package vladoff.space.lab6;

import vladoff.space.lab6.Client.Client;
import vladoff.space.lab6.Server.Server;

public class Test {
    public static void main(String[] args) {

        Thread server = new Thread(new Server("localhost"));
        Thread client1 = new Thread(new Client(6666, "localhost"));
        Thread client2 = new Thread(new Client(6668, "localhost"));

        server.start();

        client1.start();

        client2.start();



    }
}
