package vladoff.space.lab6.OrderStuff;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;

public class Orders<T extends Order & Serializable> implements Serializable {

    private List<T> orders;
    private Map<UUID, T> orderIdMap;

    public Orders(List<T> orders) {

        this.orders = Collections.synchronizedList(orders);
        this.orderIdMap = Collections.synchronizedMap(new HashMap<>());
        for(T order: orders){
            orderIdMap.put(order.getId(), order);
        }
    }

    public Orders() {
        orders = Collections.synchronizedList(new LinkedList<>());
        orderIdMap = Collections.synchronizedMap(new HashMap<>());
    }

    public void makePurchase(Credentials credentials, ShoppingCart shoppingCart) {
        T order = (T) new Order();
        //T order = (T)T.getInstance();
        order.setCredentials(credentials);
        order.setStatus(Status.WAITING);
        order.setWaitingTime(Order.DEFAULT_DAYS_TO_WAIT);
        order.setCart(shoppingCart);
        orders.add(order);
        orderIdMap.put(order.getId(), order);
    }


    public void checkOrders() {
        LocalDateTime currentTime = LocalDateTime.now();
        List<T> toRemove = new LinkedList<>();
        for(T order: orders) {
            if(currentTime.isAfter(order.getCreationTime().plusDays(order.getWaitingTime())) || order.getStatus() == Status.PROCESSED) {
                toRemove.add(order);
            }
        }
        orders.removeAll(toRemove);
    }

    public void processAllOrders() {
        for(T order: orders) {
            if(order.getStatus() == Status.WAITING) {
                order.setStatus(Status.PROCESSED);
            }
        }
    }

    public boolean findById(UUID id) {
        return orderIdMap.containsKey(id);
    }

    public T getById(UUID id) {
        if(findById(id)) {
            return orderIdMap.get(id);
        }
        return null;
    }

    public void add(T order) {
        orders.add(order);
        orderIdMap.put(order.getId(), order);
    }

    public void remove(T order) {
        orders.remove(order);
        orderIdMap.remove(order.getId(), order);
    }

    public void clear() {
        for(T order: orders) {
            orders.remove(order);
            orderIdMap.remove(order.getId(), order);
        }
    }

    public void AddAll(Orders<T> ordersToAdd) {
        List<T> list = ordersToAdd.getOrderList();
        orders.addAll(list);
        for(T order: list) {
            orderIdMap.put(order.getId(), order);
        }
    }

    public void showAllOrders() {
        for(T order: orders) {
            order.showOrder();
            System.out.println();
        }
    }

    public List<T> getOrderList() {
        return orders;
    }
}

