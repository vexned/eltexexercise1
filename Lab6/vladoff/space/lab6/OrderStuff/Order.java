package vladoff.space.lab6.OrderStuff;

import vladoff.space.lab6.Products.Product;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.UUID;

public class Order implements Serializable {
    public static final transient int DEFAULT_DAYS_TO_WAIT = 5;

    private UUID id;
    private Credentials credentials;
    private Status status;
    private LocalDateTime creationTime;
    private int waitingTime;
    private ShoppingCart<Product> cart;


    public Order(Credentials credentials, Status status, int daysToWait, ShoppingCart<Product> cart) {
        this.id = UUID.randomUUID();
        this.credentials = credentials;
        this.status = status;
        this.creationTime = LocalDateTime.now();
        this.waitingTime = daysToWait;
        this.cart = cart;
    }

    public Order()
    {
        id = UUID.randomUUID();
        status = Status.WAITING;
        creationTime = LocalDateTime.now();
        waitingTime = 5;
        cart = new ShoppingCart<>();
        credentials = new Credentials();
    }

    public static Order getInstance()
    {
        return new Order();
    }

    public Status getStatus() {
        return status;
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void showOrder() {
        credentials.read();
        System.out.println("Статус: " + status);
        System.out.println("Время ожидания (дни): " + waitingTime);
        System.out.println("Время создания: " + creationTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        System.out.println("Товары: ");
        cart.showAllObjects();
    }


    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public void setWaitingTime(int waitingTime) {
        this.waitingTime = waitingTime;
    }

    public void setCart(ShoppingCart<Product> cart) {
        this.cart = cart;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return waitingTime == order.waitingTime &&
                Objects.equals(id, order.id) &&
                Objects.equals(credentials, order.credentials) &&
                status == order.status &&
                Objects.equals(creationTime, order.creationTime) &&
                Objects.equals(cart, order.cart);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, credentials, status, creationTime, waitingTime, cart);
    }


}
