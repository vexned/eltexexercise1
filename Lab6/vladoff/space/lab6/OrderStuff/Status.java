package vladoff.space.lab6.OrderStuff;

import java.io.Serializable;

public enum Status implements Serializable {
    WAITING, PROCESSED, NOTSET;
}
