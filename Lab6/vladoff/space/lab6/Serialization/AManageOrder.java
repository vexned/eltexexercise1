package vladoff.space.lab6.Serialization;


import vladoff.space.lab6.Interfaces.IOrder;
import vladoff.space.lab6.OrderStuff.Order;
import vladoff.space.lab6.OrderStuff.Orders;

import java.io.File;
import java.nio.file.Paths;

public abstract class AManageOrder implements IOrder {
    private final String DEFAULT_FILE_PATH = Paths.get("").toAbsolutePath().toString() + File.separator + "Serialized" + File.separator;
    protected Orders<Order> orders;
    protected String fileName;

    public AManageOrder(Orders<Order> orders, String fileName) {
        this.orders = orders;
        this.fileName = DEFAULT_FILE_PATH + fileName;
    }

    public AManageOrder() {
        this.orders = new Orders<>();
        this.fileName = DEFAULT_FILE_PATH + "Default.ser";
    }
}
