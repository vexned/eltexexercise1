package vladoff.space.lab6.Interfaces;

import java.util.UUID;

public interface IOrder {
    void readById(UUID id);
    void saveById(UUID id);
    void readAll();
    void saveAll();
}
