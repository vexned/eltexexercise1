package vladoff.space.lab6.Interfaces;

public interface ICrudAction {
    void create();
    void read();
    void update();
    void delete();
}
