package controllers.OrderStuff;


import Interfaces.ICrudAction;

import java.io.Serializable;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;
import java.util.UUID;

public class Credentials implements ICrudAction, Serializable {

    private static final transient Random random = new Random();
    private static final transient String firstNames[] = {"Ivan", "Petr", "Ilya", "Erik"};
    private static final transient String lastNames[] = {"Ivanov", "Petrov", "Ilyin", "Erikov"};
    private static final transient String middleNames[] = {"Ivanovich", "Petrovich", "Ilyichov", "Erikovich"};
    private static final transient String emails[] = {"ilya@example.com", "vasya@example.com", "ilya@example.com"};

    private UUID id;
    private String lastName;
    private String firstName;
    private String middleName;
    private String email;


    public Credentials(String lastName, String firstName, String middleName, String email) {
        id = UUID.randomUUID();
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.email = email;
    }

    public  Credentials() {
        Random random = new Random();
        id = UUID.randomUUID();
        this.lastName = lastNames[random.nextInt(lastNames.length)];
        this.firstName = firstNames[random.nextInt(firstNames.length)];
        this.middleName = middleNames[random.nextInt(middleNames.length)];
        this.email = emails[random.nextInt(emails.length)];
    }

    @Override
    public void create() {
        id = UUID.randomUUID();
        this.lastName = lastNames[random.nextInt(lastNames.length)];
        this.firstName = firstNames[random.nextInt(firstNames.length)];
        this.middleName = middleNames[random.nextInt(middleNames.length)];
        this.email = emails[random.nextInt(emails.length)];
    }

    @Override
    public void read() {
        System.out.println("Имя: " + firstName);
        System.out.println("Фамилия: " + lastName);
        System.out.println("Отчество: " + middleName);
        System.out.println("E-Mail: " + email);
    }

    @Override
    public void update() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите имя:");
        firstName = scanner.next();
        System.out.println("Введите фамилию:");
        lastName = scanner.next();
        System.out.println("Введите отчество: ");
        middleName = scanner.next();
        System.out.println("Введите e-mail: ");
        email = scanner.next();
        scanner.close();
    }

    @Override
    public void delete() {
        firstName = "";
        lastName = "";
        middleName = "";
        email = "";
        id = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Credentials that = (Credentials) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(middleName, that.middleName) &&
                Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, lastName, firstName, middleName, email);
    }
}
