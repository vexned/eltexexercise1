package controllers;

import Client.Helper;
import controllers.OrderStuff.Order;
import controllers.OrderStuff.Orders;
import controllers.OrderStuff.ShoppingCart;
import controllers.Products.Product;
import Serialization.AManageOrder;
import Serialization.ManageOrderJSON;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import exception.DeleteOrderException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
public class CommandController {

    private AManageOrder manageOrder;
    private Orders<Order> orders;
    private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    CommandController() {
        orders = new Orders<>();
        manageOrder = new ManageOrderJSON(orders, "SerializedJSON.json");
    }

    @RequestMapping("/")
    public String command(@RequestParam("command") String command,
                          @RequestParam(value = "order_id", required = false) String orderId,
                          @RequestParam(value = "card_id", required = false) String cardId) throws DeleteOrderException {

        StringBuilder sb = new StringBuilder();

        switch (command) {
            case "readAll":
                manageOrder.readAll();
                List<Order> list = orders.getOrderList();
                for (Order order : list) {
                    sb.append(gson.toJson(order));
                }
                break;

            case "readById":
                if(orderId != null) {
                    UUID id = UUID.fromString(orderId);
                    manageOrder.readById(id);
                    Order order = orders.getById(id);
                    sb.append(gson.toJson(order));
                } else {
                    sb.append("Order id is not specified");
                }
                break;

            case "addToCard":
                if(cardId != null) {
                    UUID id = UUID.fromString(cardId);
                    manageOrder.readById(id);
                    Order order = orders.getById(id);
                    ShoppingCart<Product> cart = order.getCart();
                    Product product = Helper.generateProduct();
                    cart.add(product);
                    manageOrder.saveById(id);
                    sb.append(product.getId().toString());
                } else {
                    sb.append("Card id is not specified");
                }
                break;

            case "delById":
                if(orderId != null) {
                    UUID id = UUID.fromString(orderId);
                    manageOrder.readAll();
                    if(manageOrder.getFileStatus() == 1)
                        throw new DeleteOrderException("2");
                    int status = orders.removeById(id);
                    manageOrder.saveAll();
                    if(status == 1)
                        throw new DeleteOrderException("1");
                    else
                        sb.append("0");
                } else
                    throw new DeleteOrderException("3");
                break;
            default:
                break;
        }

        replaceAll(sb, Pattern.compile("\n"), "<br>");
        replaceAll(sb, Pattern.compile(" "), "&nbsp;");
        return sb.toString();
    }

    public static void replaceAll(StringBuilder sb, Pattern pattern, String replacement) {
        Matcher m = pattern.matcher(sb);
        int start = 0;
        while (m.find(start)) {
            sb.replace(m.start(), m.end(), replacement);
            start = m.start() + replacement.length();
        }
    }

    @ExceptionHandler(DeleteOrderException.class)
    public String handleException(DeleteOrderException ex) {
        StringBuilder sb = new StringBuilder();
        sb.append("An error occured during deleting an order.<br>")
                .append("Error code: ")
                .append(ex.getMessage());

        return sb.toString();
    }

}
