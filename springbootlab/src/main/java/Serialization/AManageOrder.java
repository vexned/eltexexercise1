package Serialization;


import Interfaces.IOrder;
import controllers.OrderStuff.Order;
import controllers.OrderStuff.Orders;

import java.io.File;
import java.nio.file.Paths;

public abstract class AManageOrder implements IOrder {
    private final String DEFAULT_FILE_PATH = Paths.get("").toAbsolutePath().toString() + File.separator + "Serialized" + File.separator;
    protected Orders<Order> orders;
    protected String fileName;
    protected int fileStatus;

    public AManageOrder(Orders<Order> orders, String fileName) {
        this.orders = orders;
        this.fileName = DEFAULT_FILE_PATH + fileName;
        fileStatus = 0;
    }

    public AManageOrder() {
        this.orders = new Orders<>();
        this.fileName = DEFAULT_FILE_PATH + "Default.ser";
        fileStatus = 0;
    }

    public int getFileStatus() {
        return fileStatus;
    }

    public void setFileStatus(int fileStatus) {
        this.fileStatus = fileStatus;
    }
}
