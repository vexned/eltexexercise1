package Client;

import controllers.OrderStuff.Order;
import controllers.OrderStuff.Orders;
import controllers.OrderStuff.Status;

import java.io.*;
import java.net.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Client implements Runnable {

    private static final String DEFAULT_HOSTNAME = "localhost";
    private static final int DEFAULT_WAIT_FOR_NOTIFICATION = 10000;

    private int udpPortToListen;
    private String hostname;
    private Orders<Order> orders;
    private Queue<Integer> tcpPorts;

    public Client(int udpPortToListen) {
        this(udpPortToListen, DEFAULT_HOSTNAME);
    }

    public Client(int udpPortToListen, String hostname) {
        this.udpPortToListen = udpPortToListen;
        this.hostname = hostname;
        orders = new Orders<>();
        tcpPorts = new ConcurrentLinkedQueue<>();
    }

    public void waitForNotification() {
        try (DatagramSocket socket = new DatagramSocket(udpPortToListen)) {
            while (true) {
                try {
                    DatagramPacket request = new DatagramPacket(new byte[4096], 4096);
                    socket.receive(request);
                    byte[] receivedBytes = request.getData();
                    InputStream is = new ByteArrayInputStream(receivedBytes);
                    ObjectInputStream ois = new ObjectInputStream(is);
                    Object receivedObject = ois.readObject();

                    if (receivedObject instanceof String) {
                        UUID orderId = UUID.fromString((String) receivedObject);
                        Order order = orders.getById(orderId);
                        order.setStatus(Status.PROCESSED);
                        LocalDateTime time = LocalDateTime.now();
                        System.out.println("[" + udpPortToListen + "][CLIENT]ЗАКАЗ ОБРАБОТАН(" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + "): ");
                        order.showOrder();
                    } else {
                        Integer tcpPort = (Integer) receivedObject;
                        tcpPorts.add((Integer) receivedObject);
                        System.out.println("[" + udpPortToListen + "][CLIENT]" + "ПОЛУЧЕН ПОРТ НА СОЕДИНЕНИЕ И ОТПРАВКУ: " + tcpPort);
                    }
                } catch (IOException | RuntimeException | ClassNotFoundException ex) {
                    System.out.println("[CLIENT]" + ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    void sendThroughTcp(int interval) {
        Thread send = new Thread(() -> {
            while (true) {
                Integer tcpPort = tcpPorts.poll();
                while (tcpPort != null) {
                    try {
                        Socket clientSocket = new Socket();
                        clientSocket.setSoTimeout(20000);
                        clientSocket.connect(new InetSocketAddress(InetAddress.getByName(hostname), tcpPort));
                        ObjectOutputStream ous = new ObjectOutputStream(clientSocket.getOutputStream());
                        Order order = generateOrder();
                        ous.writeObject(order);
                        orders.add(order);
                        clientSocket.close();
                        System.out.println("[" + udpPortToListen + "][CLIENT]ВЫСЫЛАЕМ ЗАКАЗ ("+ order.getId().toString() + ") НА " + tcpPort);
                    } catch (IOException e) {
                        System.out.println("[CLIENT][TCPSEND] " + e.getMessage() + ". Retry.");
                        tcpPorts.add(tcpPort);
                        e.printStackTrace();
                    } finally {
                        tcpPort = tcpPorts.poll();
                    }

                }
                try {
                    Thread.sleep(interval);
                } catch (InterruptedException e) {
                    System.out.println(e.getMessage());
                }
            }
        });
        send.start();
    }

    @Override
    public void run() {
        sendThroughTcp(20000);
        waitForNotification();

    }

    private Order generateOrder() {
        return Helper.generateOrder();
    }
}
