package Server;

import controllers.OrderStuff.Order;
import controllers.OrderStuff.Orders;

public class WaitingCheck<T extends Order> extends ACheck<T> {

    WaitingCheck(Orders<T> orders, long interval) {
        super(orders, interval);
    }

    @Override
    void Process() throws InterruptedException {
        while(true) {
                orders.processAllOrders();
                System.out.println("Обработка заказов в ожидании.");
                Thread.sleep(interval);
                System.out.println("Заказы в ожидании обработаны.");
        }
    }

}
