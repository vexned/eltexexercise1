package vladoff.space;

public class Main {

    public static void main(String[] args) {
        firstExercise(args);
    }

    //Демонстрация первой лабораторной работы
    public static void firstExercise(String[] args) {
        int count = Integer.parseInt(args[0]);
        String type = args[1];

        Product[] products;

        if(type.equals("Coffee")){
            products = new Coffee[count];
            for(int i = 0; i < count; i++) {
                products[i] = new Coffee();
                products[i].create();
                products[i].update();
                products[i].read();
            }
        }
        else if(type.equals("Tea")) {
            products = new Tea[count];
            for(int i = 0; i < count; i++) {
                products[i] = new Tea();
                products[i].create();
                products[i].update();
                products[i].read();
            }
        }
        else {
            System.exit(1);
        }

    }
}
