package vladoff.space;

import java.util.Objects;
import java.util.Scanner;

public final class Tea extends Product {

    private String packagingType;

    Tea() {
        super();
    }

    @Override
    public void create() {
        super.create();
        packagingType = "Unknown";
    }

    @Override
    public void read() {
        super.read();
        System.out.println("Тип упаковки: " + packagingType);
    }

    @Override
    public void update() {
        super.update();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите тип упаковки: ");
        packagingType = scanner.next();
    }

    @Override
    public void delete() {
        super.delete();
        packagingType = "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Tea tea = (Tea) o;
        return Objects.equals(packagingType, tea.packagingType);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), packagingType);
    }
}
