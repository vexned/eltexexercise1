package vladoff.space;

public interface ICrudAction {
    void create();
    void read();
    void update();
    void delete();
}
