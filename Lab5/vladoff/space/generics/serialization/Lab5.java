package vladoff.space.generics.serialization;

import vladoff.space.generics.serialization.Products.Coffee;
import vladoff.space.generics.serialization.Products.Product;
import vladoff.space.generics.serialization.Products.Tea;

public class Lab5 {
    public static void main(String[] args) {

        Credentials c1 = new Credentials();
        c1.create();
        Credentials c2 = new Credentials();
        c2.create();
        Credentials c3 = new Credentials();
        c3.create();


        Product p1 = new Tea();
        p1.create();
        Product p2 = new Coffee();
        p2.create();
        Product p3 = new Coffee();
        p3.create();

        ShoppingCart<Product> sc1 = new ShoppingCart<>();
        ShoppingCart<Product> sc2 = new ShoppingCart<>();
        ShoppingCart<Product> sc3 = new ShoppingCart<>();

        sc1.add(p1);
        sc1.add(p2);

        sc2.add(p3);
        sc2.add(p1);

        sc3.add(p2);
        sc3.add(p3);

        Orders<Order> orders = new Orders<>();

        orders.makePurchase(c1, sc1);
        orders.makePurchase(c2, sc2);
        orders.makePurchase(c3, sc3);

        AManageOrder mo1 = new ManageOrderFile(orders, "Serialized.ser");
        AManageOrder mo2 = new ManageOrderJSON(orders, "SerializedJSON.json");

        mo1.saveAll();
        mo2.saveAll();

        Orders<Order> ordersFromBinary = new Orders<>();
        Orders<Order> ordersFromJSON = new Orders<>();

        AManageOrder mo3 = new ManageOrderFile(ordersFromBinary, "Serialized.ser");
        AManageOrder mo4 = new ManageOrderJSON(ordersFromJSON, "SerializedJSON.json");

        mo3.readAll();
        mo4.readAll();
    }
}
