package vladoff.space.generics.serialization.Interfaces;

public interface ICrudAction {
    void create();
    void read();
    void update();
    void delete();
}
