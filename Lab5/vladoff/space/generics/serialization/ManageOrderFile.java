package vladoff.space.generics.serialization;


import java.io.*;
import java.util.UUID;

public class ManageOrderFile extends AManageOrder {

    public ManageOrderFile(Orders<Order> orders, String fileName) {
        super(orders, fileName);
    }

    public ManageOrderFile() {
        super();
    }

    @Override
    public void readById(UUID id) {
        Orders<Order> ordersToRead;
        try {
            FileInputStream fis = new FileInputStream(fileName);
            ObjectInputStream ois = new ObjectInputStream(fis);
            ordersToRead = (Orders<Order>) ois.readObject();
            ois.close();
            fis.close();
            Order order = ordersToRead.getById(id);
            if(order == null)
            {
                System.out.println("Object with following UUID not found!");
                return;
            }
            orders.add(order);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Specified file not found!");
        } catch (IOException e) {
            System.out.println("Something bad happened to IO Subsystem.");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Following class not found!");
        }
    }

    @Override
    public void saveById(UUID id) {
        Orders<Order> ordersToRead;
        try {
            FileInputStream fis = new FileInputStream(fileName);
            ObjectInputStream ois = new ObjectInputStream(fis);
            ordersToRead = (Orders<Order>) ois.readObject();
            ois.close();
            fis.close();
            Order order = ordersToRead.getById(id);
            if(order != null)
                ordersToRead.remove(order);

            Order orderToSave = orders.getById(id);
            if(orderToSave == null)
            {
                System.out.println("Order with following id is not found!");
                return;
            }
            ordersToRead.add(orderToSave);
            FileOutputStream fos = new FileOutputStream(fileName);
            ObjectOutputStream ous = new ObjectOutputStream(fos);
            ous.writeObject(ordersToRead);
            ous.close();
            fos.close();
        } catch (FileNotFoundException e) {
            System.out.println("Specified file not found!");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Something bad happened to IO Subsystem.");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Following class not found!");
        }
    }

    @Override
    public void readAll() {
        try {
            FileInputStream fis = new FileInputStream(fileName);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Orders<Order> ordersToRead = (Orders<Order>) ois.readObject();
            orders.clear();
            orders.AddAll(ordersToRead);
        } catch (FileNotFoundException e) {
            System.out.println("Specified file not found!");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Something bad happened to IO Subsystem.");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("Following class not found!");
            e.printStackTrace();
        }
    }

    @Override
    public void saveAll() {
        try {
            FileOutputStream fos = new FileOutputStream(fileName);
            ObjectOutputStream ous = new ObjectOutputStream(fos);
            ous.writeObject(orders);
            ous.close();
            fos.close();
        } catch (FileNotFoundException e) {
            System.out.println("Specified file not found!");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Something bad happened to IO Subsystem.");
            e.printStackTrace();
        }
    }
}
