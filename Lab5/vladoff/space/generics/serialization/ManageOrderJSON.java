package vladoff.space.generics.serialization;

import com.google.gson.Gson;
import com.sun.org.apache.xpath.internal.operations.Or;

import java.io.*;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.UUID;

public class ManageOrderJSON extends AManageOrder {

    private final Gson gson = new Gson();

    public ManageOrderJSON(Orders<Order> orders, String fileName) {
        super(orders, fileName);
    }

    public ManageOrderJSON() {
        super();
    }

    @Override
    public void readById(UUID id) {
        try {
            Reader reader = new FileReader(fileName);
            Orders<Order> ordersToRead = gson.fromJson(reader, getType(Orders.class, Order.class));
            reader.close();
            Order orderToRead = ordersToRead.getById(id);
            if(orderToRead == null) {
                System.out.println("There is no order with this id");
                return;
            }

            if(orders.findById(id)) {
                orders.remove(orderToRead);
            }

            orders.add(orderToRead);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void saveById(UUID id) {
        Order orderToSave = orders.getById(id);
        if(orderToSave == null)
        {
            System.out.println("Following order not found");
            return;
        }

        try {
            Reader reader = new FileReader(fileName);
            Orders<Order> ordersToRead = gson.fromJson(reader, getType(Orders.class, Order.class));
            reader.close();
            Order orderToRemove = ordersToRead.getById(id);
            if(orderToRemove != null)
                ordersToRead.remove(orderToRemove);
            ordersToRead.add(orderToSave);
            Writer writer = new FileWriter(fileName);
            gson.toJson(ordersToRead, writer);
            writer.close();

        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Something bad happened to IO");
            e.printStackTrace();
        }

    }

    @Override
    public void readAll() {
        try {
            Reader reader = new FileReader(fileName);
            Orders<Order> ordersToRead;
                                                                                          ordersToRead = gson.fromJson(reader, getType(Orders.class, Order.class));
            orders.clear();
            orders.AddAll(ordersToRead);
            reader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Following file not found!");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Something bad happened to IO");
            e.printStackTrace();
        }
    }

    @Override
    public void saveAll() {
        try {
            Writer writer = new FileWriter(fileName);
            gson.toJson(orders, writer);
            writer.close();
        } catch (IOException e) {
            System.out.println("Something bad happened to IO");
            e.printStackTrace();
        }
    }

    private Type getType(Class<?> rawClass, Class<?> parameter) {
        return new ParameterizedType() {
            @Override
            public Type[] getActualTypeArguments() {
                return new Type[] {parameter};
            }
            @Override
            public Type getRawType() {
                return rawClass;
            }
            @Override
            public Type getOwnerType() {
                return null;
            }
        };
    }
}
