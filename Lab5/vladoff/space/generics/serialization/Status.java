package vladoff.space.generics.serialization;

import java.io.Serializable;

public enum Status implements Serializable {
    WAITING, PROCESSED, NOTSET;
}
