package controllers.OrderStuff;


import controllers.Products.Product;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;


@Entity
@Table(name = "shoppingCart")
@Access(AccessType.FIELD)
public class ShoppingCart<T extends Product> implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Type(type="uuid-char")
    UUID id;

    @Transient
    List<T> products;

    @ElementCollection
    @CollectionTable(name="productsIDs", joinColumns=@JoinColumn(name="shoppingCart_id"))
    @Column(name="productID")
    Set<UUID> productID;

    public ShoppingCart() {
        id = UUID.randomUUID();
        products = new ArrayList<>();
        productID = new HashSet<>();
    }

    public ShoppingCart(List<T> products) {
        id = UUID.randomUUID();
        this.products = products;
        productID = new HashSet<>();
        for(T product: products) {
            productID.add(product.getId());
        }
    }

    public void add(T product) {
        products.add(product);
        productID.add(product.getId());
    }

    public void delete(T product) {
        products.remove(product);
        productID.remove(product.getId());
    }

    public boolean findInShoppingCart(UUID id) {
        return productID.contains(id);
    }

    public void showAllObjects() {
        for(Product product : products) {
            product.read();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShoppingCart<?> that = (ShoppingCart<?>) o;
        return Objects.equals(products, that.products) &&
                Objects.equals(productID, that.productID);
    }

    @Override
    public int hashCode() {

        return Objects.hash(products, productID);
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @OneToMany(orphanRemoval = true,
    cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @Access(AccessType.PROPERTY)
    public List<Product> getProducts() {
        List<Product> teas = new ArrayList<>();
        teas.addAll(products);
        return teas;
    }

    public void setProducts(List<Product> products) {
        List<T> prods = new ArrayList<>();
        Set<UUID> newProdId = new HashSet<>();
        for(Product product: products) {
            newProdId.add(product.getId());
            prods.add((T) product);
        }
        this.products = prods;
        this.productID = newProdId;
    }

    public Set<UUID> getProductID() {
        return productID;
    }

    public void setProductID(Set<UUID> productID) {
        this.productID = productID;
    }
}
