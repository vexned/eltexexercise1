package controllers.OrderStuff;

import controllers.Products.Product;
import org.hibernate.annotations.Type;
import util.JSONify;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.UUID;


@Entity(name = "uorders")
public class Order implements Serializable {

    @Transient
    public static final transient int DEFAULT_DAYS_TO_WAIT = 5;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Type(type="uuid-char")
    private UUID id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="credentials_id")
    private Credentials credentials;

    @Enumerated(EnumType.STRING)
    private Status status;
    private LocalDateTime creationTime;
    private int waitingTime;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="cart_id")
    private ShoppingCart<Product> cart;


    public Order(Credentials credentials, Status status, int daysToWait, ShoppingCart<Product> cart) {
        this.id = UUID.randomUUID();
        this.credentials = credentials;
        this.status = status;
        this.creationTime = LocalDateTime.now();
        this.waitingTime = daysToWait;
        this.cart = cart;
    }

    public Order()
    {
        id = UUID.randomUUID();
        status = Status.WAITING;
        creationTime = LocalDateTime.now();
        waitingTime = 5;
        cart = new ShoppingCart<>();
        credentials = new Credentials();
    }

    public static Order getInstance()
    {
        return new Order();
    }

    public Status getStatus() {
        return status;
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void showOrder() {
        credentials.read();
        System.out.println("Статус: " + status);
        System.out.println("Время ожидания (дни): " + waitingTime);
        System.out.println("Время создания: " + creationTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        System.out.println("Товары: ");
        cart.showAllObjects();
    }


    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public void setWaitingTime(int waitingTime) {
        this.waitingTime = waitingTime;
    }

    public void setCart(ShoppingCart<Product> cart) {
        this.cart = cart;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return waitingTime == order.waitingTime &&
                Objects.equals(id, order.id) &&
                Objects.equals(credentials, order.credentials) &&
                status == order.status &&
                Objects.equals(creationTime, order.creationTime) &&
                Objects.equals(cart, order.cart);
    }

    public ShoppingCart<Product> getCart() {
        return cart;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, status, waitingTime, cart, credentials, creationTime);
        //cart
        //credentials
        //, creationTime
    }

    @Override
    public String toString() {
        return JSONify.toJson(this);
    }


}
