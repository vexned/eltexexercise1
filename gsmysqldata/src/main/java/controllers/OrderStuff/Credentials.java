package controllers.OrderStuff;


import Interfaces.ICrudAction;
import org.hibernate.annotations.Type;
import util.JSONify;
import util.RandomDataGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;
import java.util.Scanner;
import java.util.UUID;

@Entity
public class Credentials implements ICrudAction, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Type(type="uuid-char")
    private UUID id;

    private String lastName;
    private String firstName;
    private String middleName;
    private String email;


    public Credentials(String lastName, String firstName, String middleName, String email) {
        id = UUID.randomUUID();
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.email = email;
    }

    public  Credentials() {
        create();
    }

    @Override
    public void create() {
        this.id = UUID.randomUUID();
        this.firstName = RandomDataGenerator.getFirstName();
        this.lastName = RandomDataGenerator.getLastName();
        this.middleName = RandomDataGenerator.getMiddleName();
        this.email = RandomDataGenerator.getEmail();
    }

    @Override
    public void read() {
        System.out.println("Имя: " + firstName);
        System.out.println("Фамилия: " + lastName);
        System.out.println("Отчество: " + middleName);
        System.out.println("E-Mail: " + email);
    }

    @Override
    public void update() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите имя:");
        firstName = scanner.next();
        System.out.println("Введите фамилию:");
        lastName = scanner.next();
        System.out.println("Введите отчество: ");
        middleName = scanner.next();
        System.out.println("Введите e-mail: ");
        email = scanner.next();
        scanner.close();
    }

    @Override
    public void delete() {
        firstName = "";
        lastName = "";
        middleName = "";
        email = "";
        id = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Credentials that = (Credentials) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(middleName, that.middleName) &&
                Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, lastName, firstName, middleName, email);
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return JSONify.toJson(this);
    }
}
