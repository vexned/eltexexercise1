package controllers.Products;

import util.JSONify;
import util.RandomDataGenerator;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;
import java.util.Scanner;

@Embeddable
@Entity(name = "Tea")
@DiscriminatorValue("TEA")
public final class Tea extends Product {

    private String packagingType;

    public Tea() {
        create();
    }

    @Override
    public void create() {
        super.create();
        name = RandomDataGenerator.getTeaName();
        packagingType = RandomDataGenerator.getPackagingType();
    }

    @Override
    public void read() {
        super.read();
        System.out.println("Тип упаковки: " + packagingType);
    }

    @Override
    public void update() {
        super.update();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите тип упаковки: ");
        packagingType = scanner.next();
    }

    @Override
    public void delete() {
        super.delete();
        packagingType = "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Tea tea = (Tea) o;
        return Objects.equals(packagingType, tea.packagingType);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), packagingType);
    }

    public String getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }

    @Override
    public String toString() {
        return JSONify.toJson(this);
    }
}
