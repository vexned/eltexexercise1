package controllers.Products;

import Interfaces.ICrudAction;
import org.hibernate.annotations.Type;
import util.JSONify;
import util.RandomDataGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.Scanner;
import java.util.UUID;


@Entity
@Table(name = "product")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "PRODUCT_CLASS_NAME")
public class Product implements ICrudAction, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Type(type="uuid-char")
    private UUID id;

    protected String name;
    private double price;
    private static transient int productCounter;
    private String provider;
    private String country;

    public Product(){
        create();
    }

    @Override
    public void create() {
        productCounter++;
        name = RandomDataGenerator.getProductName();
        price = RandomDataGenerator.getPrice();
        country = RandomDataGenerator.getCountry();
        provider = RandomDataGenerator.getProvider();
    }

    @Override
    public void read() {
        System.out.println("Наименование: " + name);
        System.out.println("Цена: " + price);
        System.out.println("Поставщик: " + provider);
        System.out.println("Страна: " + country);
        System.out.println("Общее число товаров:" + productCounter);
    }

    @Override
    public void update() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите наименование: ");
        name = scanner.next();
        System.out.print("Введите цену: ");
        price = scanner.nextDouble();
        System.out.print("Введите поставщика: ");
        provider = scanner.next();
        System.out.print("Введите страну: ");
        country = scanner.next();
    }

    @Override
    public void delete() {
        productCounter--;
        name = "";
        id = null;
        price = 0.0d;
        provider = "";
        country = "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Double.compare(product.price, price) == 0 &&
                Objects.equals(id, product.id) &&
                Objects.equals(name, product.name) &&
                Objects.equals(provider, product.provider) &&
                Objects.equals(country, product.country);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, price, provider, country);
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public static int getProductCounter() {
        return productCounter;
    }

    public static void setProductCounter(int productCounter) {
        Product.productCounter = productCounter;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return JSONify.toJson(this);
    }
}
