package controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import controllers.OrderStuff.Order;
import controllers.OrderStuff.ShoppingCart;
import controllers.Products.Product;
import exception.DeleteOrderException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import util.Helper;

import java.util.Optional;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
public class CommandController {

    @Autowired
    private OrderRepository orderRepository;

    private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    CommandController() {
    }

    @RequestMapping("/")
    public String command(@RequestParam("command") String command,
                          @RequestParam(value = "order_id", required = false) String orderId,
                          @RequestParam(value = "card_id", required = false) String cardId) throws DeleteOrderException {

        StringBuilder sb = new StringBuilder();

        switch (command) {
            case "readAll":
                Iterable<Order> orders = orderRepository.findAll();
                for (Order order : orders) {
                    sb.append(gson.toJson(order));
                }
                break;
            case "readById":
                if(orderId != null) {
                    Order order = null;
                    UUID id = UUID.fromString(cardId);
                    Optional<Order> optionalOrder = orderRepository.findById(id);
                    if(optionalOrder.isPresent())
                    {
                        order = optionalOrder.get();
                    }
                    if(order == null) {
                        sb.append("Following order not found");
                        break;
                    }
                    sb.append(gson.toJson(order));
                } else {
                    sb.append("Order id is not specified");
                }
                break;

            case "addToCard":
                if(cardId != null) {
                    Order order = null;
                    UUID id = UUID.fromString(cardId);
                    Optional<Order> optionalOrder = orderRepository.findById(id);
                    if(optionalOrder.isPresent())
                    {
                        order = optionalOrder.get();
                    }
                    if(order == null) {
                        sb.append("Following order not found!");
                        break;
                    }
                    ShoppingCart<Product> cart = order.getCart();
                    Product product = Helper.generateProduct();
                    cart.add(product);
                    orderRepository.deleteById(id);
                    orderRepository.save(order);
                    sb.append(product.getId().toString());
                    break;
                } else {
                    sb.append("Card id is not specified");
                    break;
                }
            case "delById":
                if(orderId != null) {
                    UUID id = UUID.fromString(orderId);
                    orderRepository.deleteById(id);
                    sb.append("0");
                } else
                    throw new DeleteOrderException("3");
                break;
            case "addOrder":
                Order order = Helper.generateOrder();
                orderRepository.save(order);
                sb.append("Order created");
                break;
            default:
                break;
        }

        replaceAll(sb, Pattern.compile("\n"), "<br>");
        replaceAll(sb, Pattern.compile(" "), "&nbsp;");
        return sb.toString();
    }

    public static void replaceAll(StringBuilder sb, Pattern pattern, String replacement) {
        Matcher m = pattern.matcher(sb);
        int start = 0;
        while (m.find(start)) {
            sb.replace(m.start(), m.end(), replacement);
            start = m.start() + replacement.length();
        }
    }

    @ExceptionHandler(DeleteOrderException.class)
    public String handleException(DeleteOrderException ex) {
        StringBuilder sb = new StringBuilder();
        sb.append("An error occured during deleting an order.<br>")
                .append("Error code: ")
                .append(ex.getMessage());

        return sb.toString();
    }

}
