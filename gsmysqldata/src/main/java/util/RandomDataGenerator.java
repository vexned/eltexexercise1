package util;

import java.util.Random;

public class RandomDataGenerator {

    private static final transient Random random = new Random();
    private static final transient String firstNames[] = {"Ivan", "Petr", "Ilya", "Erik"};
    private static final transient String lastNames[] = {"Ivanov", "Petrov", "Ilyin", "Erikov"};
    private static final transient String middleNames[] = {"Ivanovich", "Petrovich", "Ilyichov", "Erikovich"};
    private static final transient String emails[] = {"ilya@example.com", "vasya@example.com", "ilya@example.com"};
    private final static transient String names[] = {"Утренний", "Вечерний", "Хороший", "Неплохой", "Крутой"};
    private final static transient String coffeeNames[] = {"Nescafe", "Jacobs", "Jardin", "Jockey", "Cafe Creme", "Meseta"};
    private final static transient String TeaNames[] = {"Brooke Bond", "TESS", "Twinings", "Althaus", "Curtis", "Майский чай", "Greenfield", "Ahmad", "Lipton", "Пиала", "Ассам"};
    private final static transient String providers[] = {"CompanyCo", "CompCo", "CoffeGradesCo", "NiceAndCoolCompany"};
    private final static transient String countries[] = {"Россия", "Беларусь", "Казахстан", "Коста-Рика", "Испания", "Германия"};
    private static final transient String packagingTypes[] = {"Саше", "Трехшовный", "Дой-пак", "Зип", "Жестяная банка"};
    private final static transient String coffeeGrades[] = {"Арабика", "Робуста", "Либерика", "Эксельсо"};

    public static String getCoffeeGrade() {
        return fromArray(coffeeGrades);
    }

    public static String getPackagingType() {
        return fromArray(packagingTypes);
    }

    public static double getPrice() {
        return (double)(int)(random.nextDouble()*1000);
    }

    public static String getCountry() {
        return  fromArray(countries);
    }

    public static String getProvider() {
        return fromArray(providers);
    }

    public static String getTeaName() {
        return fromArray(TeaNames);
    }

    public static String getCoffeeName() {
        return fromArray(coffeeNames);
    }

    public static String getProductName() {
        return fromArray(names);
    }

    public static String getFirstName() {
        return fromArray(firstNames);
    }

    public static String getLastName() {
        return fromArray(lastNames);
    }

    public static String getMiddleName() {
        return fromArray(middleNames);
    }

    public static String getEmail() {
        return fromArray(emails);
    }

    private static String fromArray(String stringArray[]) {
        return  stringArray[random.nextInt(stringArray.length)];
    }
}
