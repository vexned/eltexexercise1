package util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JSONify {

    private static Gson json = new GsonBuilder().create();

    public static String toJson(Object object) {
        return json.toJson(object);
    }
}
