package vladoff.space.generics;


import vladoff.space.generics.Products.Product;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;


public class ShoppingCart<T extends Product> {
    List<T> products;
    HashSet<UUID> productID;

    public ShoppingCart() {
        products = new ArrayList<>();
        productID = new HashSet<>();
    }

    public ShoppingCart(List<T> products) {
        this.products = products;
        productID = new HashSet<>();
        for(T product: products) {
            productID.add(product.getId());
        }
    }

    public void add(T product) {
        products.add(product);
        productID.add(product.getId());
    }

    public void delete(T product) {
        products.remove(product);
        productID.remove(product.getId());
    }

    public boolean findInShoppingCart(UUID id) {
        return productID.contains(id);
    }

    public void showAllObjects() {
        for(Product product : products) {
            product.read();
        }
    }
}
