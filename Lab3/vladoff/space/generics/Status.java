package vladoff.space.generics;

public enum Status {
    WAITING, PROCESSED, NOTSET;
}
