package vladoff.space.generics;

import vladoff.space.generics.Products.Coffee;
import vladoff.space.generics.Products.Product;
import vladoff.space.generics.Products.Tea;

public class Lab3 {
    public static void main(String[] args) {
        //Создаем коллекцию заказов
        Orders<Order> orders = new Orders<>();

        //Создаем данные пользователей
        Credentials ivanov = new Credentials("Иванов", "Иван", "Иванович", "example@example.com");
        Credentials petrov = new Credentials("Петров", "Петр", "Петрович", "petrov@example.com");

        //Производим ввод продуктов, которые будут добавлены в корзину
        System.out.println("Введите данные продуктов (Чай, Кофе): ");

        Product tea = new Tea();
        tea.create();
        tea.update();

        Product coffee = new Coffee();
        coffee.create();
        coffee.update();

        System.out.println();
        //Создаем корзины для Петрова и Иванова
        System.out.println("Создаем корзины для Иванова и Петрова: ");
        ShoppingCart<Product> ivanovCart = new ShoppingCart<>();
        ShoppingCart<Product> petrovCart = new ShoppingCart<>();



        //Добавляем продукты в корзины
        System.out.println("Добавляем Иванову кофе, Петрову чай");
        ivanovCart.add(coffee);
        petrovCart.add(tea);

        System.out.println("У Иванова есть чай?" + ((ivanovCart.findInShoppingCart(tea.getId())) ? "Да" : "Нет"));
        System.out.println("У Иванова есть кофе?" + ((ivanovCart.findInShoppingCart(coffee.getId())) ? "Да" : "Нет"));

        System.out.println("У Петрова есть чай?" + ((petrovCart.findInShoppingCart(tea.getId())) ? "Да" : "Нет"));
        System.out.println("У Петрова есть кофе?" + ((petrovCart.findInShoppingCart(coffee.getId())) ? "Да" : "Нет"));
        System.out.println();

        //Печатаем содержимое корзин
        System.out.println("Корзина Иванова: ");
        ivanovCart.showAllObjects();
        System.out.println();
        System.out.println("Корзина Петрова: ");
        petrovCart.showAllObjects();
        System.out.println();


        //Наполняем коллекцию заказов
        orders.makePurchase(ivanov, ivanovCart);
        orders.makePurchase(petrov, petrovCart);
        //Просроченный заказ для Петрова
        Order petrovOrder = new Order(petrov, Status.PROCESSED, 0, petrovCart);
        orders.add(petrovOrder);


        System.out.println("Печатаем весь список заказов: ");
        orders.showAllOrders();

        System.out.println();
        //Завершаем все заказы
        orders.checkOrders();

        //Смотри заказы
        orders.showAllOrders();

        //Удаляем чай из корзины петрова
        petrovCart.delete(tea);
        System.out.println("Печатаем пустую корзину петрова");
        petrovCart.showAllObjects();
    }
}
