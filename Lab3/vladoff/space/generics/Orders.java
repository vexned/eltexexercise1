package vladoff.space.generics;

import java.time.LocalDateTime;
import java.util.*;

public class Orders<T extends Order> {

    private List<T> orders;
    private Map<LocalDateTime, T> timeCheckingHash;

    public Orders(List<T> orders) {

        this.orders = Collections.synchronizedList(orders);
        timeCheckingHash = Collections.synchronizedMap(new HashMap<>());
        for(T order: orders){
            timeCheckingHash.put(order.getCreationTime(), order);
        }
    }

    public Orders() {
        orders = Collections.synchronizedList(new LinkedList<>());
        timeCheckingHash = Collections.synchronizedMap(new HashMap<>());
    }

    public void makePurchase(Credentials credentials, ShoppingCart shoppingCart) {
        T order = (T) new Order();
        //T order = (T)T.getInstance();
        order.setCredentials(credentials);
        order.setStatus(Status.WAITING);
        order.setWaitingTime(Order.DEFAULT_DAYS_TO_WAIT);
        order.setCart(shoppingCart);
        orders.add(order);
        timeCheckingHash.put(order.getCreationTime(), order);
    }


    public void checkOrders() {
        LocalDateTime currentTime = LocalDateTime.now();
        List<T> toRemove = new LinkedList<>();
        for(T order: orders) {
            if(currentTime.isAfter(order.getCreationTime().plusDays(order.getWaitingTime())) || order.getStatus() == Status.PROCESSED) {
                toRemove.add(order);
            }
        }
        orders.removeAll(toRemove);
    }

    public void processAllOrders() {
        for(T order: orders) {
            if(order.getStatus() == Status.WAITING) {
                order.setStatus(Status.PROCESSED);
            }
        }
    }

    public void add(T order) {
        orders.add(order);
        timeCheckingHash.put(order.getCreationTime(), order);
    }

    public void showAllOrders() {
        for(T order: orders) {
            order.showOrder();
            System.out.println();
        }
    }
}

