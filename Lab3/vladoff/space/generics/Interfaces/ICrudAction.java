package vladoff.space.generics.Interfaces;

public interface ICrudAction {
    void create();
    void read();
    void update();
    void delete();
}
