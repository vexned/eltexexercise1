package vladoff.space;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;


public class ShoppingCart {
    List<Product> products;
    HashSet<UUID> productID;

    ShoppingCart() {
        products = new ArrayList<>();
        productID = new HashSet<>();
    }

    ShoppingCart(List<Product> products) {
        this.products = products;
        productID = new HashSet<>();
        for(Product product: products) {
            productID.add(product.getId());
        }
    }

    public void add(Product product) {
        products.add(product);
        productID.add(product.getId());
    }

    public void delete(Product product) {
        products.remove(product);
        productID.remove(product.getId());
    }

    public boolean findInShoppingCart(UUID id) {
        return productID.contains(id);
    }

    public void showAllObjects() {
        for(Product product : products) {
            product.read();
        }
    }
}
