package vladoff.space;

import java.util.HashMap;

public enum Status {
    WAITING, PROCESSED, NOTSET;
}
