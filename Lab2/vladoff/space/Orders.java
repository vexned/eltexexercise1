package vladoff.space;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Orders {

    private List<Order> orders;
    private Map<LocalDateTime, Order> timeCheckingHash;

    public Orders(List<Order> orders) {
        this.orders = orders;
        timeCheckingHash = new HashMap<>();
        for(Order order: orders){
            timeCheckingHash.put(order.getCreationTime(), order);
        }
    }

    public Orders() {
        orders = new LinkedList<>();
        timeCheckingHash = new HashMap<>();
    }

    public void makePurchase(Credentials credentials, ShoppingCart shoppingCart) {
        Order order = new Order(credentials, Status.WAITING, Order.DEFAULT_DAYS_TO_WAIT, shoppingCart);
        orders.add(order);
        timeCheckingHash.put(order.getCreationTime(), order);
    }


    public void checkOrders() {
        LocalDateTime currentTime = LocalDateTime.now();
        for(Order order: orders) {
            if(currentTime.isAfter(order.getCreationTime().plusDays(order.getWaitingTime())) && order.getStatus() == Status.PROCESSED) {
                orders.remove(order);
            }
        }
    }

    public void add(Order order) {
        orders.add(order);
        timeCheckingHash.put(order.getCreationTime(), order);
    }

    public void showAllOrders() {
        for(Order order: orders) {
            order.showOrder();
            System.out.println();
        }
    }
}
