package vladoff.space;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Order {
    public static final int DEFAULT_DAYS_TO_WAIT = 5;

    private Credentials credentials;
    private Status status;
    private LocalDateTime creationTime;
    private int waitingTime;
    private ShoppingCart cart;


    public Order(Credentials credentials, Status status, int daysToWait, ShoppingCart cart) {
        this.credentials = credentials;
        this.status = status;
        this.creationTime = LocalDateTime.now();
        this.waitingTime = daysToWait;
        this.cart = cart;
    }

    public Order()
    {
        status = Status.WAITING;
        creationTime = LocalDateTime.now();
        waitingTime = 5;
        cart = new ShoppingCart();
        credentials = new Credentials();
    }

    public Status getStatus() {
        return status;
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void showOrder() {
        credentials.read();
        System.out.println("Статус: " + status);
        System.out.println("Время ожидания (дни): " + waitingTime);
        System.out.println("Время создания: " + creationTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        System.out.println("Товары: ");
        cart.showAllObjects();
    }
}
