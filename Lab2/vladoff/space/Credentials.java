package vladoff.space;

import java.util.Scanner;
import java.util.UUID;

public class Credentials implements ICrudAction {
    private UUID id;
    private String lastName;
    private String firstName;
    private String middleName;
    private String email;


    public Credentials(String lastName, String firstName, String middleName, String email) {
        id = UUID.randomUUID();
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.email = email;
    }

    public  Credentials() {
        id = UUID.randomUUID();
        this.lastName = "None";
        this.firstName = "None";
        this.middleName = "None";
        this.email = "None";
    }

    @Override
    public void create() {
        id = UUID.randomUUID();
        this.lastName = "None";
        this.firstName = "None";
        this.middleName = "None";
        this.email = "None";
    }

    @Override
    public void read() {
        System.out.println("Имя: " + firstName);
        System.out.println("Фамилия: " + lastName);
        System.out.println("Отчество: " + middleName);
        System.out.println("E-Mail: " + email);
    }

    @Override
    public void update() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите имя:");
        firstName = scanner.next();
        System.out.println("Введите фамилию:");
        lastName = scanner.next();
        System.out.println("Введите отчество: ");
        middleName = scanner.next();
        System.out.println("Введите e-mail: ");
        email = scanner.next();
        scanner.close();
    }

    @Override
    public void delete() {
        firstName = "";
        lastName = "";
        middleName = "";
        email = "";
        id = null;
    }
}
