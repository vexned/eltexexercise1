import vladoff.space.generics.Order;
import vladoff.space.generics.Orders;

public class Lab4 {
    public static void main(String[] args) {

        Orders<Order> orders = new Orders<>();

        ACheck<Order> processedCheck = new ProcessedCheck<>(orders, 2000);
        ACheck<Order> waitingCheck = new WaitingCheck<>(orders, 2000);

        Helper<Order> helper = new Helper<>(orders, 1000);

        Thread t1 = new Thread(processedCheck);
        t1.setName("processedThread");
        t1.start();
        Thread t2 = new Thread(waitingCheck);
        t2.setName("waitingThread");
        t2.start();

        helper.setName("HelperThread");
        helper.start();
    }
}
