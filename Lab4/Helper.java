import vladoff.space.generics.Credentials;
import vladoff.space.generics.Order;
import vladoff.space.generics.Orders;
import vladoff.space.generics.Products.Coffee;
import vladoff.space.generics.Products.Product;
import vladoff.space.generics.Products.Tea;
import vladoff.space.generics.ShoppingCart;


import java.util.Random;

public class Helper<T extends Order> extends Thread {

    private final static int MAX_ITEMS_IN_CART = 5;
    private final static Random random = new Random();
    Orders<T> orders;
    long interval;

    public Helper(Orders<T> orders, long interval) {
        this.orders = orders;
        this.interval = interval;
    }

    @Override
    public void run() {
        super.run();
        while (true) {
            Credentials credentials = new Credentials();
            credentials.create();
            ShoppingCart<Product> shoppingCart = new ShoppingCart<>();
            Product product;
            int itemsNumber = random.nextInt(MAX_ITEMS_IN_CART);
            for(int i = 0; i < itemsNumber; i++);
            {
                int productNumber = random.nextInt(2);
                if(productNumber==0)
                {
                    product = new Tea();
                    product.create();
                }
                else
                {
                    product = new Coffee();
                    product.create();
                }

                shoppingCart.add(product);
            }

            System.out.println("Order created! Order contains " + (itemsNumber+1) + " items.");

            //credentials.read();
            //shoppingCart.showAllObjects();

            orders.makePurchase(credentials, shoppingCart);

            try {
                sleep(interval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
