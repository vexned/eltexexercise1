import vladoff.space.generics.Order;
import vladoff.space.generics.Orders;

import java.util.Scanner;

public class ProcessedCheck<T extends Order> extends ACheck<T> {

    ProcessedCheck(Orders<T> orders, int interval) {
        super(orders, interval);
    }


    @Override
    void Process() throws InterruptedException {

        while(true){
                orders.checkOrders();
                System.out.println("Обработка завершенных заказов.");
                Thread.sleep(interval);
                System.out.println("Завершенные заказы обработаны");
        }
    }
}
